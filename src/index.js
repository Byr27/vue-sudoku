import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';

// libs
import { MdButton } from 'vue-material/dist/components';
import 'vue-material/dist/vue-material.min.css';
import 'normalize.css';


import App from './App';
import store from './store';

Vue.use(MdButton);
Vue.use(Vuex);
Vue.use(VueResource);

Vue.config.devtools = true;

const app = new Vue({
  store,
  render: h => h(App),
});

app.$mount('#app');
