import Vue from 'vue';
import Vuex, { Store } from 'vuex';

import state from './state';

Vue.use(Vuex);

export default new Store({
  state,
});
